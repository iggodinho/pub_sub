#!/usr/bin/env python
import rospy
from std_msgs.msg import Int64

def callback(data):
    print("Variable = %s"%str(data.data))

def listener():
    rospy.init_node("listener", anonymous=True)
    rospy.Subscriber("chatter", Int64, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
