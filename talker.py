#!/usr/bin/env python

import rospy
from std_msgs.msg import Int64

def counter():
    pub = rospy.Publisher('chatter', Int64, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        for c in range(10):
            n = c + 1
            rospy.loginfo(n)
            pub.publish(n)
            rate.sleep()
if __name__ == '__main__':
    try:
        counter()
    except rospy.ROSInterruptException:
        pass
