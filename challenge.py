#!/usr/bin/env python

import rospy
from std_msgs.msg import Int64

def counter():
    pub = rospy.Publisher('chatter', Int64, queue_size=10)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
      for c in range(10):
        n=c+1
        pub.publish(n)
        rate.sleep()

def callback(data):
    if data.data %2 != 0:
        print(data.data)

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('chatter', Int64, callback)

if __name__ == '__main__':
    try:
      listener()
      counter()
    except rospy.ROSInterruptException:
      pass